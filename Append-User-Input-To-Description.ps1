#User Input to Description Script

#Declare Variables

$userInputArray = @()
$wiObject = $null
$currentDescription = ""
$updatedDescriptionArray = @()
$updatedObject = $null

#load modules
import-module smlets

#wi with lots of input
#$testWI = (get-scsmobject -Class (Get-SCSMClass -name "System.WorkItem.ServiceRequest$") -Filter "ID -eq SR28150").get_ID()
#$wiGUID = $testWI
$wiGUID = "<SCORCH VAR HERE>"
$wiObject = get-scsmobject -ID $wiGUID


Function fn-get-user-input
{
    Param (
            $wiObject
            )

    #Declare Vars
    $userInput = ""
    $Array = @()
    $ListArray = @()

    #### From Blog
    #### http://itblog.no/4462
    $UserInput = $wiObject.UserInput
    $nl = [Environment]::NewLine
    $content=[XML]$UserInput
    $inputs = $content.UserInputs.UserInput
    foreach ($input in $inputs)
    {
        if($($input.Answer) -like "<value*")
        {       
            [xml]$answer = $input.answer
            foreach($value in $($($answer.values)))
            {
                    foreach($item in $value)
                    {                   
                        foreach ($txt in $($item.value))
                        {                     
                            $ListArray += $($txt.DisplayName)                  
                        }
                        $Array += $input.Question + " = " + [string]::Join(", ",$ListArray)
                        $ListArray = $null
                    }
            }
        }
        else 
        {
             if ($input.type -eq "enum")
            {
                $ListGuid = Get-SCSMEnumeration -Id $input.Answer
                $Array += $($input.Question + " = " + $ListGuid.displayname)
            }
            else 
            {
            $Array += $($input.Question + " = " + $input.Answer)
            }
        }
    }
    $Array
}

Function fn-get-current-description
{
    param(
        $wiObject
        )

        $wiFieldValues = $wiObject.values
        $wiDescription = $wiFieldValues | ?{$_.type -match "Description"} | select value 
        $wiDescriptionString = $wiDescription.value

        $wiDescriptionString

}

Function fn-create-new-decription
{
    param(
        $userInput,
        $currentDescription
        )

        #Declare Vars
        $DescriptionArray = @()

        $DescriptionArray += "************ Begin User Input Information **********"
        $DescriptionArray += ""
        $DescriptionArray += $userInput
        $DescriptionArray += ""
        $DescriptionArray += "************ End User Input Information **********"
        $DescriptionArray += ""
        $DescriptionArray += "************ Begin Original Description **********"
        $DescriptionArray += ""
        $DescriptionArray += $currentDescription

        $DescriptionArray

}

Function fn-update-object
{
    param(
        $wiObject,
        $updatedDescription,
        $currentDescription
        )

       $updatedDescription = [string]::Join("`r`n",$updatedDescriptionArray)


        if ($currentDescription -notmatch "Begin User Input Information")
        {
            $wiObject | Set-SCSMObject -Property "Description" -Value $updatedDescription
        }

}


#### Main ####

$userInputArray = fn-get-user-input -wiObject $wiObject

if ($userInputArray -ne $null)
{

    $currentDescription = fn-get-current-description -wiObject $wiObject
    $updatedDescriptionArray = fn-create-new-decription -userInput $userInputArray -currentDescription $currentDescription
    $updatedObject = fn-update-object -wiObject $wiObject -currentDescription $currentDescription -updatedDescription $updatedDescription #Remove whitepsace in User Input Array when writing new description

}